Shader "Projector/Multiply" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (0,0,0,1)
		_ShadowTex ("Cookie", 2D) = "gray" { TexGen ObjectLinear }
	}

	Subshader 
	{
	  Tags { "RenderType"="Transparent-1" }
	  Pass 
	  {
	     ZWrite Off
	     Fog { Color (1, 1, 1) }
	     Blend DstColor Zero
		 Offset -1, -1
	     SetTexture [_ShadowTex] 
	     {
	        combine one - texture alpha
	        Matrix [_Projector]
	     }
	      SetTexture [_ShadowTex] 
	     {
	        ConstantColor [_Color]
	        Combine previous + Constant
	     }
	    
	  }
	}
}