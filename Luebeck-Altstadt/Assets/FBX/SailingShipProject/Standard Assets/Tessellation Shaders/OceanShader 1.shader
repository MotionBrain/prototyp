// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Ship/Tessellation/Water (displacement)"
{
Properties 
{
	_Color ("Color", Color) = (0.5, 0.5, 0.5, 1)
	_Color1 ("Color1", Color) = (0.5, 0.5, 0.5, 1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", float) = 0.078125
	_Sharp ("_Sharp", Range (0, 1)) = 0.078125
	_Parallax ("Height", float) = 0.0
	_Parallax1 ("Height1", float) = 0.0
	_Tiling ("Tiling", float) = 1
	_Tiling1 ("Tiling1", float) = 1
	_Tiling2 ("Tiling2", float) = 1	
	_BumpStr1 ("Bump Str.",  Range (0,2)) = 0
	_BumpMap ("Normalmap", 2D) = "bump" {}
	
	_BumpStr2 ("Bump Str2.",  Range (0, 2)) = 0
	_BumpMap0 ("Normalmap1", 2D) = "bump" {}
	
	_BumpStr3 ("Bump Str3.",  Range (0, 2)) = 0
	_BumpMap1 ("Normalmap2", 2D) = "bump" {}
	
	_ParallaxMap1 ("Heightmap (A)", 2D) = "black" {}
	_ParallaxMap2 ("Heightmap2 (A)", 2D) = "black" {}
	
	//_EdgeLength ("Edge length", Range(3,50)) = 10
	_Scrolls ("Scrolls", Vector) = (0,0,0,0)
	
	_Tess ("Tessellation", Range(1,32)) = 4
	_minDist ("minDist", float) = 10
	_maxDist ("maxDist", float) = 25
	
	_FresnelTex ("_FresnelTex", 2D) = "black" {}
	_fresnelPower ("_fresnelPower", float) = 1
	_FoamTex ("_FoamTex", 2D) = "black" {}

	_FoamSize  ("_FoamSize", Range (0.00, 1)) = 0.0
	_FoamScroll ("_FoamScroll", float) = 0.0
	_Tiling3 ("_FoamTiling", float) = 0.0
	_WavesDepth ("_WavesDepth", Range (0.00, 1) ) = 0.0
	
	_ShipFoamTex ("_ShipFoamTex", 2D) = "black" {}
	_ShipFoam_ParallaxMap ("_ShipFoam_ParallaxMap", 2D) = "black" {}
	_ShipFoamDepth ("_ShipFoamDepth", float) = 0.0
}

SubShader 
{ 
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	//  zwrite off
	//Blend off
	CGPROGRAM

	#pragma surface surf BlinnPhong addshadow vertex:disp tessellate:tessDistance nolightmap 
	#include "Tessellation.cginc"
	
	sampler2D _BumpMap,_BumpMap1,_BumpMap0;
	sampler2D _FresnelTex,_FoamTex;
	sampler2D _ShipFoam_ParallaxMap, _ShipFoamTex; 
	//sampler2D _CameraDepthTexture;
	sampler2D _ReflectionTex;
	
	sampler2D _ParallaxMap1,_ParallaxMap2;
	
	fixed4 _Color,_Color1,_ReflectionTint;
	fixed _Shininess;
	fixed _Tiling,_Tiling1,_Tiling2,_Tiling3;
	fixed _Parallax,_Parallax1;
	fixed _ParallaxOffset;
	fixed _EdgeLength;
	fixed4 _Scrolls;
	fixed _Tess;
	fixed _minDist;
	fixed _maxDist;
	fixed _BumpStr1, _BumpStr2, _BumpStr3;
	fixed _Sharp;

    fixed _FoamSize;
    fixed _FoamParallax;
    fixed _FoamScroll;
    fixed _WavesDepth;
    fixed _ShipFoamDepth;   
    fixed _ReflDistort;
    fixed _fresnelPower;

	

	struct appdata 
	{
		fixed4 vertex : POSITION;
		fixed4 tangent : TANGENT;
		fixed3 normal : NORMAL;
		fixed2 texcoord : TEXCOORD0;
		fixed2 texcoord1 : TEXCOORD1;
	};
	
	
	struct Input 
	{
		fixed4 position  : POSITION;
		fixed2 uv_ShipFoamTex;
		fixed3 worldPos;
        float3 viewDir;
	};
	
	
	
	fixed4 tessDistance (appdata v0, appdata v1, appdata v2) 
	{
		return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, _minDist, _maxDist, _Tess);
	}


	void disp (inout appdata v)
	{
		fixed3 _UV = mul((fixed4x4)unity_ObjectToWorld, v.vertex.xyz);
		fixed3 _UV1 = _UV;
		fixed2 _UV2 = _UV.xy;
		_UV.xz *= _Tiling * 0.01;
		_UV.xz += _Scrolls.xy * _Time;
		_UV1.xz *= _Tiling1 * 0.01;
		_UV1.xz += _Scrolls.zw * _Time;
		_UV2 *= _Tiling3 * 0.01;;
		_UV2.x += _FoamScroll * _Time;
		
		fixed d = (tex2Dlod(_ParallaxMap1, fixed4(_UV.xz,0,0)).a ) * _Parallax * 2 - .25;
		fixed d1 = (tex2Dlod(_ParallaxMap2, fixed4(_UV1.xz,0,0)).a ) * _Parallax1 * 2 - .25;
		fixed d2 = (tex2Dlod(_ShipFoam_ParallaxMap, fixed4(v.texcoord.xy,0,0)).a) * _ShipFoamDepth;
		fixed2 d2_1 = (tex2Dlod(_ShipFoamTex, fixed4( v.texcoord.xy,0,0)).rg);

		d2 *= d2_1;
		fixed foamMask = (max(d+d1-_FoamSize,0)) * _FoamParallax;
		
		fixed h = (d + d1) * .5;
		v.vertex.y += v.normal.y * h + (foamMask  + d2); 
		
	}	
	
	void surf (Input IN, inout SurfaceOutput o) 
	{
	


       
	
		fixed2 _uv = IN.worldPos.xz;
		fixed2 _uv1 = _uv;
		fixed2 _uv2 = _uv;
		_uv *= _Tiling * 0.01;
		
		_uv1 = IN.worldPos.xz;
		_uv1 *= _Tiling1 * 0.01;
		
		_uv += _Scrolls.xy * _Time;
		_uv1 += _Scrolls.zw * _Time;
		
		_uv2 *= _Tiling3 * 0.01;
		_uv2.x += _FoamScroll * _Time;
		
		
		fixed lerpMask = tex2D(_ParallaxMap2, _uv1).a;
		lerpMask = lerpMask + 1 * 0.5;
	 	fixed3 n1 = UnpackNormal(tex2D(_BumpMap, _uv));
	 	n1 = lerp(fixed3(0,0,1),n1,_BumpStr1);
		fixed3 n2 = UnpackNormal(tex2D(_BumpMap0, _uv1));
		n2 = lerp(fixed3(0,0,1),n2,_BumpStr2);
		fixed3 n3 = UnpackNormal(tex2D(_BumpMap1, _uv * _Tiling2));
		n3 = lerp(fixed3(0,0,1),n3,_BumpStr3);
		o.Normal = (n1 + n2 + n3) / 3;
		half fresnelFac = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
		

		//fixed fresnel = sqrt(1.0 - dot(-normalize(worldView), worldNormal));
		fresnelFac = pow (fresnelFac, _fresnelPower);
		fresnelFac = (0.8 * fresnelFac + 0.2) * _Color.a;
		 
		
		fixed4 _FrTex = tex2D(_FresnelTex, fixed2(fresnelFac,0.5));
		
		o.Albedo = _FrTex.rgb * _Color.rgb;
		
		o.Emission += _FrTex.a * _Color1.rgb;

		fixed4 _F = tex2D(_ParallaxMap1, _uv);
		fixed4 _F1 = tex2D(_ParallaxMap2, _uv1);
		fixed4 _FTex = tex2D(_FoamTex, _uv2);
		fixed _SFTex = tex2D(_ShipFoam_ParallaxMap, IN.uv_ShipFoamTex).a;
		IN.uv_ShipFoamTex.y -= _FoamScroll * _Time * 0.15;
		fixed2 _SFTex1 = tex2D(_ShipFoamTex, IN.uv_ShipFoamTex).rg;
		fixed foamMask =  max(_F+_F1-_FoamSize,0);
		
		o.Albedo = lerp(_Color.rgb,_Color1.rgb, fresnelFac);
		o.Albedo = lerp(o.Albedo, _FTex, foamMask * 2 * _FTex.a + (_SFTex * (_SFTex1.r + _SFTex1.g))) * saturate(_F+_F1+_WavesDepth) ;

		o.Alpha = 1;
		o.Specular = _Shininess;
		o.Gloss = saturate(_Color.a - (foamMask * 8 * _FTex.a));	
		
		
	}
ENDCG
}

}

