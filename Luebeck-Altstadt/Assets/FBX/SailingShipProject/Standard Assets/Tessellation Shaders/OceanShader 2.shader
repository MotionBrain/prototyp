Shader "Tessellation/Water (displacement)"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Float ) = 0.078125
		_Hardness ("_Hardness", Float ) = 0.078125
		_Radius ("_Radius", Float ) = 0.078125
	
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}
		
		_ParallaxMap ("Heightmap (RG)", 2D) = "black" {}
		_Parallax ("Height", float ) = 0.5
		_Parallax2 ("Height2", float ) = 0.5

		_EdgeLength ("Detail", Range(5,50)) = 10
		_ParallaxT1 ("Displacement Small Wawes " , Vector ) = ( 1, 1, 0,-0 )
		_ParallaxT2 ("Displacement Middle Wawes " , Vector ) = ( 1, 1, 0,-0 )
		_TextureTile ("Textures size" , Vector ) = ( 1, 1, 0,-0 )
		_Blend ("Height2", Range (0.0, 1.0)) = 0.5
	}
	
SubShader
{
	Tags { "RenderType"="Opaque" }
	LOD 800

	CGPROGRAM
	#pragma surface surf Blinn addshadow vertex:vert tessellate:tessEdge
	#include "Tessellation.cginc"

	fixed _Hardness,_Radius;
	inline fixed4 LightingBlinn(SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
	{
		half3 h = normalize (lightDir + viewDir);
		
		h*= _Hardness;
		h -= _Radius;
		
		fixed diff = max (0, dot (s.Normal, lightDir));
		
		float nh = max (0, dot (s.Normal, h));
		float spec = pow (nh, s.Specular*128.0) * s.Gloss;
		
		fixed4 c;
		c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * _SpecColor.rgb * spec) * (atten * 2);
		c.a = s.Alpha + _LightColor0.a * _SpecColor.a * spec * atten;
		return c;
	}


	struct appdata
	{
		float4 vertex : POSITION;
		float4 tangent : TANGENT;
		float3 normal : NORMAL;
		float2 texcoord : TEXCOORD0;
		float2 texcoord1 : TEXCOORD1;
	};

	float _EdgeLength;
	float _Parallax, _Parallax2;



	sampler2D _MainTex;
	sampler2D _BumpMap;
	fixed4 _Color,_ParallaxT1, _ParallaxT2;
	half _Shininess, _Blend;
	
	
	
	

	struct Input
	{
		float2 uv_MainTex;
		float2 uv_BumpMap;
		float2 uv1;
	};

	float4 tessEdge (appdata v0, appdata v1, appdata v2)
	{
		return UnityEdgeLengthBasedTessCull (v0.vertex, v1.vertex, v2.vertex,  _EdgeLength, (_Parallax + _Parallax2) * 1.5f);
	}

	sampler2D _ParallaxMap;

	void vert(inout appdata v)
	{

		float d = (tex2Dlod(_ParallaxMap, float4(v.texcoord.xy * _ParallaxT1.xy + _ParallaxT1.zw * _Time ,0,0)).r * _Parallax) * 2 - 1;;
		float d1 = (tex2Dlod(_ParallaxMap, float4(v.texcoord.xy * _ParallaxT2.xy + _ParallaxT2.zw * _Time ,0,0)).g * _Parallax2) * 2 - 1;

		v.vertex.xyz += v.normal * ((d + d1) - ((_Parallax2 - _Parallax) / 2)) ;
	}

	void surf (Input IN, inout SurfaceOutput o)
	{

		fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = tex.rgb * _Color.rgb;
		o.Gloss = tex.a;
		o.Alpha = tex.a * _Color.a;
		o.Specular = _Shininess;
		half3 n1 = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap * _ParallaxT1.xy + _ParallaxT1.zw * _Time));
		half3 n2 = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

		o.Normal =n1;
	}
	ENDCG
}

FallBack "Bumped Specular"
}






