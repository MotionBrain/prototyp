Shader "Ship/Tessellation/Alpha Test (displacement)" 
{
	Properties 
	{
		
		_Alpha ("Alpha", Float) = 1
		_Gamma("Gamma", Float) = 1
		_ParallaxOffset ("Height Offset", float) = 0.0
		_Parallax ("Height", float) = 0.0
		_ColorMap ("_ColorMap (RGB) Gloss (A)", 2D) = "grey" {}
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_ParallaxMap ("Heightmap (A)", 2D) = "black" {}
	
		 _Tess ("Tessellation", Range(1,32)) = 4
	}
	
SubShader 
{ 
	Tags {"Queue"="Alpha" "IgnoreProjector"="True" "RenderType"="Transparent"}
	LOD 800
	cull off
	
	CGPROGRAM
	#pragma surface surf Lambert alpha vertex:disp  tessellate:tessDistance
	#include "Tessellation.cginc"

	
	struct appdata 
	{
		fixed4 vertex : POSITION;
		fixed4 tangent : TANGENT;
		fixed3 normal : NORMAL;
		fixed2 texcoord : TEXCOORD0;
		fixed2 texcoord1 : TEXCOORD1;
	};
	
	fixed _Parallax, _ParallaxOffset, _Tess, _Alpha, _Gamma;
	
	
	float4 tessDistance (appdata v0, appdata v1, appdata v2) 
	{
	    float minDist = 10.0;
	    float maxDist = 25.0;
	    return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, minDist, maxDist, _Tess);
	}
	
	sampler2D _ParallaxMap;
		
	void disp (inout appdata v)
	{
		fixed d = (tex2Dlod(_ParallaxMap, float4(v.texcoord.xy,0,0)).a + _ParallaxOffset) * _Parallax ;
		d -= _Parallax * .5;
		v.vertex.xyz += v.normal * d;
	}
	
	sampler2D _MainTex, _ColorMap;
	
	struct Input 
	{
		fixed2 uv_MainTex;
		fixed2 uv_ColorMap;
	};
	
	void surf (Input IN, inout SurfaceOutput o) 
	{
		fixed4 colmap = tex2D(_ColorMap, IN.uv_ColorMap);
		fixed tex = tex2D(_MainTex, IN.uv_MainTex).a * _Alpha;
		o.Albedo = (colmap.rgb * _Gamma);
		o.Alpha = tex;
	}
	ENDCG
}

}
