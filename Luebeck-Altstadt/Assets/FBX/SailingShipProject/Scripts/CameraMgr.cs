using UnityEngine;
using System.Collections;

public class CameraMgr : MonoBehaviour 
{
	public GameObject m_logo_object;
	public GameObject[] m_GO;
	public GameObject m_EndFade;
	private int mLastNum = 0;
	public Transform mPlayer;
	public Transform mResp;
	
	void Start () 
	{
		
	}
	
	void Update () 
	{
		if(Input.GetMouseButtonDown(0))
		{

			Screen.lockCursor = true;
			m_EndFade.SetActive(false);
			m_EndFade.SetActive(true);
			m_logo_object.SetActive(false);
			int length = m_GO.Length;
			for (int i = 0; i < length; i++)
				m_GO[i].active = i == mLastNum;
			
			mLastNum = (mLastNum == (length - 1)) ? 0 : mLastNum + 1;
			
			mPlayer.position = mResp.position;
			
		}
	}
}
