using UnityEngine;
using System.Collections;

public class MusicMGR : MonoBehaviour 
{
	public AudioSource m_Source;
	public AudioClip m_Clip;
	void Update () 
	{
		if (m_Source.isPlaying != true)
		{
			m_Source.clip = m_Clip;
			m_Source.Play();
		}
	}
}
