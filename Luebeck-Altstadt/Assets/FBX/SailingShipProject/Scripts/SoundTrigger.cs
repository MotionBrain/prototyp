using UnityEngine;
using System.Collections;

public class SoundTrigger : MonoBehaviour 
{

	void OnTriggerEnter () 
	{
		GetComponent<AudioSource>().Play();
	}
}
