using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour 
{
	private bool m_menu = false;
	public GameObject m_CamMgr;
	public GameObject m_BlackScreen;
	public GameObject m_QualitySwicher;
	public GameObject m_Gui;
	
	void Start()
	{
		m_BlackScreen.SetActive(false);	
	}
	
    void Update() 
	{
        if (Input.GetKeyDown("escape"))
		{
			if (m_menu )
			{
				Time.timeScale = 1.0F;
				Screen.lockCursor = true;
				m_menu = false;
				m_CamMgr.SetActive(true);
				m_BlackScreen.SetActive(false);
				m_QualitySwicher.SetActive(false);
			}
			
			else
			{
				Time.timeScale = 0.01F;
				Screen.lockCursor = false;		
				m_menu = true;
				m_CamMgr.SetActive(false);
				m_BlackScreen.SetActive(true);
				m_QualitySwicher.SetActive(true);
			}
			
		}
        
    }
}