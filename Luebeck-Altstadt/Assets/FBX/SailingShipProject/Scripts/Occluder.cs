using UnityEngine;
using System.Collections;

public class Occluder : MonoBehaviour 
{
	public GameObject m_CurObject;
	public GameObject m_PrevObject;
	
	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.layer == 2 && m_PrevObject.activeSelf)
		{
			Debug.Log ("Collide OnTriggerEnter " + gameObject.name);
			m_PrevObject.SetActive(false);
			m_CurObject.SetActive(true);
		}
		
	}
	/*
	void OnTriggerExit(Collider other) 
	{
		if (other.gameObject.layer == 2 && !m_PrevObject.activeSelf)
		{
			Debug.Log ("Collide OnTriggerExit " + gameObject.name);
			m_PrevObject.SetActive(true);
			m_CurObject.SetActive(false);
		}
		
	}*/
	
	void OnTriggerStay(Collider other) 
	{
		if (other.gameObject.layer == 2 && m_PrevObject.activeSelf)
		{
			//Debug.Log ("Collide OnTriggerStay " + gameObject.name);
			m_PrevObject.SetActive(false);
			m_CurObject.SetActive(true);
		}
			
	}
}
