using UnityEngine;
using System.Collections;

public class TimeDestructor : MonoBehaviour 
{
	public float m_Time = 1.0f;

	void Start () 
	{	
		Destroy(this.gameObject, m_Time);
	
	}
	
}
