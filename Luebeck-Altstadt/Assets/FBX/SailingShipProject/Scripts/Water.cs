using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour 
{

	public Transform mResp;
	public GameObject m_EndFade;
	void OnTriggerEnter(Collider other) 
	
	{
		if (other.gameObject.layer == 2 )
		{
			m_EndFade.SetActive(false);
			m_EndFade.SetActive(true);
			other.transform.position = mResp.position;
		}
	}
}
