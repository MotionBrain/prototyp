using UnityEngine;
using System.Collections;

public class FocusPoint : MonoBehaviour 
{
	public Transform m_Transform;
	public float smooth = 1.0f;

	//private  float distanceToGround;

    void Update() 
	{
		RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
		
        if (Physics.Raycast(transform.position, fwd, out hit))
	    	m_Transform.position = Vector3.Lerp(m_Transform.position, hit.point, Time.deltaTime * smooth);      
    }
}
