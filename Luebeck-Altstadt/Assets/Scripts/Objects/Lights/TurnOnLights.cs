﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
namespace JoachimLipka
{
    /// <summary>
    /// 
    /// </summary>
    public class TurnOnLights : MonoBehaviour
    {
        /// <summary>
        /// 
        /// </summary>
        public GameObject[] lights;
        /// <summary>
        /// 
        /// </summary>
        public GameObject[] outerCore;


        public GameObject LightController;

        /// <summary>
        /// 
        /// </summary>
        private bool lightStatus = false;

        public void TurnOnLight()
        {
            if(lightStatus)
            {
                changeStatusOfLight(false);
                lightStatus = false;
                LightController.GetComponent<LightController>().RemoveLight();
            }
            else
            {
                changeStatusOfLight(true);
                lightStatus = true;
                LightController.GetComponent<LightController>().AddLight();
            }

        }

        private void changeStatusOfLight(bool status)
        {
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].SetActive(status);
            }
            for (int i = 0; i < outerCore.Length; i++)
            {
                outerCore[i].SetActive(status);
            }
        }
    }
}
