﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
namespace JoachimLipka
{
    /// <summary>
    /// 
    /// </summary>
    public class LightController : MonoBehaviour
    {
        /// <summary>
        /// reference to the searchable lights to finish this mini game
        /// </summary>
        public int searchableLights;

        /// <summary>
        /// reference to the number of turned on lights
        /// </summary>
        private int i = 0;

        private bool lightGameFinished = false;

        /// <summary>
        /// if a light is turned on,
        /// the index i increases by one
        /// </summary>
        public void AddLight()
        {
            i++;
            if (i == searchableLights && !lightGameFinished)
            {
                lightGameFinished = true;
                Debug.Log("Let it shine");
            }
        }

        /// <summary>
        /// if a light is turned off,
        /// the index i decreases by one
        /// </summary>
        public void RemoveLight()
        {
            i--;
        }
    }
}
