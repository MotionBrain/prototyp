﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace JoachimLipka
{
    public class ProduceRag : MonoBehaviour
    {

        public int resourceIndex;
        public int produceIndex;

        public float resourceTimer;
        // Update is called once per frame
        void Update()
        {
            resourceTimer += Time.deltaTime;
            if (resourceTimer > 10)
            {
                resourceTimer = 0;
                if (GetComponent<CityStorage>().wool >= resourceIndex)
                {
                    GetComponent<CityStorage>().wool -= resourceIndex;
                    GetComponent<CityStorage>().rag += produceIndex;
                    GetComponent<CityStorage>().SetResourcesToUi();
                }
            }
        }
    }
}
