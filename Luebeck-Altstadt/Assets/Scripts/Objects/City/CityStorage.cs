﻿using UnityEngine;
using UnityEngine.UI;

namespace JoachimLipka
{
    public class CityStorage : MonoBehaviour
    {
        /// <summary>
        /// capacity of wood
        /// </summary>
        public int wood;

        /// <summary>
        /// capacity of wool
        /// </summary>
        public int wool;

        /// <summary>
        /// capacity of cloth
        /// </summary>
        public int rag;

        /// <summary>
        ///  reference to the woods text element
        /// </summary>
        public Text woodText;

        /// <summary>
        ///  reference to the wool text element
        /// </summary>
        public Text woolText;

        /// <summary>
        ///  reference to the rag text element
        /// </summary>
        public Text ragText;

        // Update is called once per frame
        void Start()
        {
            SetResourcesToUi();
        }

        /// <summary>
        /// calls the function, which sets the current amunt of resources to the UI
        /// </summary>
        public void SetResourcesToUi()
        {
            SetResources(woodText, wood);
            SetResources(woolText, wool);
            SetResources(ragText, rag);
        }

        /// <summary>
        /// sets the resources to the ui
        /// </summary>
        /// <param name="text"> reference to the text element </param>
        /// <param name="resource"> reference to the resource source</param>
        public void SetResources(Text text, int resource)
        {
            text.text = "" + resource;
        }

        /// <summary>
        /// fills up the resources from the ship
        /// </summary>
        /// <param name="woodStorage"> reference to the woodstorage </param>
        /// <param name="woolStorage"> reference to the woolstorage </param>
        /// <param name="ragStorage"> reference to the ragstorage </param>
        public void FillUpResources(int woodStorage, int woolStorage, int ragStorage)
        {
            wood = wood + woodStorage;

            wool = wool + woolStorage;

            rag = rag + ragStorage;

            SetResourcesToUi();
        }

        /// <summary>
        /// loads up the ship with resources
        /// </summary>
        /// <param name="ship"> references to the ship </param>
        public void LoadUpShip(GameObject ship, Transform targetPier)
        {
            ship.GetComponent<ShipStorage>().SetStorage(wood/5, wool, rag, targetPier);
            wood = wood - (wood / 5);
            wool = wool - (wool);
            rag = rag - (rag);
        }
    }
}
