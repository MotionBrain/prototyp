﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace JoachimLipka
{
    public class SendShip : MonoBehaviour
    {
        /// <summary>
        /// 
        /// </summary>
        public GameObject ship;

        /// <summary>
        /// 
        /// </summary>
        public List<GameObject> city = new List<GameObject>();

        //
        public GameObject LuebeckToYork;
        public GameObject YorkToLuebeck;
        public GameObject LuebeckToBruegge;
        public GameObject BrueggeToLuebeck;
        public GameObject BrueggeToYork;
        public GameObject YorkToBruegge;

        /// <summary>
        /// 
        /// </summary>
        public void SendResources()
        {
            if(city[0] && city[1] && city[0].name != city[1].name)
            {
                GameObject newShip = Instantiate(ship);
                newShip.SetActive(true);
                city[0].GetComponent<CityStorage>().LoadUpShip(newShip, city[1].transform);
                if (city[0].name == "Luebeck" && city[1].name == "York")
                {
                    GameObject newRoute = Instantiate(LuebeckToYork);
                    newRoute.GetComponent<CPC_CameraPath>().selectedObject = newShip;
                    newRoute.GetComponent<CPC_CameraPath>().PlayingTrue();
                }
                else if (city[0].name == "York" && city[1].name == "Luebeck")
                {
                    GameObject newRoute = Instantiate(YorkToLuebeck);
                    newRoute.GetComponent<CPC_CameraPath>().selectedObject = newShip;
                    newRoute.GetComponent<CPC_CameraPath>().PlayingTrue();

                }
                else if (city[0].name == "York" && city[1].name == "Bruegge")
                {
                    GameObject newRoute = Instantiate(YorkToBruegge);
                    newRoute.GetComponent<CPC_CameraPath>().selectedObject = newShip;
                    newRoute.GetComponent<CPC_CameraPath>().PlayingTrue();
                }
                else if (city[0].name == "Bruegge" && city[1].name == "York")
                {
                    GameObject newRoute = Instantiate(BrueggeToYork);
                    newRoute.GetComponent<CPC_CameraPath>().selectedObject = newShip;
                    newRoute.GetComponent<CPC_CameraPath>().PlayingTrue();
                }
                else if (city[0].name == "Bruegge" && city[1].name == "Luebeck")
                {
                    GameObject newRoute = Instantiate(BrueggeToLuebeck);
                    newRoute.GetComponent<CPC_CameraPath>().selectedObject = newShip;
                    newRoute.GetComponent<CPC_CameraPath>().PlayingTrue();
                }
                else if (city[0].name == "Luebeck" && city[1].name == "Bruegge")
                {
                    GameObject newRoute = Instantiate(LuebeckToBruegge);
                    newRoute.GetComponent<CPC_CameraPath>().selectedObject = newShip;
                    newRoute.GetComponent<CPC_CameraPath>().PlayingTrue();
                }
                city[0].GetComponent<CityStorage>().SetResourcesToUi();
            }
            else
            {
                city[0] = null;
                city[1] = null;
            }
        }

    }
}