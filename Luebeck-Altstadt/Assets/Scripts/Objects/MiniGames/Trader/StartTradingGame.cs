﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace JoachimLipka
{
    public class StartTradingGame : MonoBehaviour
    {
        public GameObject[] buildings;


        // turn off the game setup
        void Start()
        {
            // turn off the game setup
            for (int i = 0; i < buildings.Length; i++)
            {
                buildings[i].SetActive(false);
                buildings[i].GetComponent<CityStorage>().enabled = false;
            }
        }

        /// <summary>
        /// starts the mini trade game
        /// </summary>
        public void StartGame()
        {
            for (int i = 0; i < buildings.Length; i++)
            {
                buildings[i].SetActive(true);
                buildings[i].GetComponent<CityStorage>().enabled = true;
            }
        }
    }
}
