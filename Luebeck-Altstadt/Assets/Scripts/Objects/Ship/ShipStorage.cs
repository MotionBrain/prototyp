﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JoachimLipka
{
    public class ShipStorage : MonoBehaviour
    {
        /// <summary>
        /// capacity of wood
        /// </summary>
        public int storedWood;

        /// <summary>
        /// capacity of wool
        /// </summary>
        public int storedWool;

        /// <summary>
        /// capacity of cloth
        /// </summary>
        public int storedRag;

        private Transform targetPier;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wood"></param>
        /// <param name="wool"></param>
        /// <param name="cloth"></param>
        public void SetStorage(int wood, int wool, int rag, Transform pier)
        {
            storedWood = wood;
            storedWool = wool;
            storedRag = rag;
            targetPier = pier;
        }
        
        public void EmptyStorage()
        {
            targetPier.GetComponent<CityStorage>().FillUpResources(storedWood, storedWool, storedRag);
            ClearStorage();
        }

        public void ClearStorage()
        {
            storedWood = 0;
            storedWool = 0;
            storedRag = 0;
            Destroy(gameObject);
        }
    }
}
