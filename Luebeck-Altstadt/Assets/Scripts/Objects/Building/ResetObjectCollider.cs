﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
namespace JoachimLipka
{
    /// <summary>
    /// if any object collids with this collider
    /// the object will be set back
    /// </summary>
    public class ResetObjectCollider : MonoBehaviour
    {

        /// <summary>
        /// if the object is entering the collider
        /// the function set object back will be called
        /// </summary>
        /// <param name="other"> the object which got throw by the user </param>
        public void OnTriggerEnter(Collider other)
        {
            other.GetComponent<ResetObjects>().SetObjectBack();
        }
    }
}
