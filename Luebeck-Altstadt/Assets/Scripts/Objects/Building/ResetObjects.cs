﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
namespace JoachimLipka
{
    /// <summary>
    /// saves the start position of each object 
    /// and sets the objects back to the origin position
    /// </summary>
    public class ResetObjects : MonoBehaviour
    {

        /// <summary>
        /// reference to the start rotation
        /// </summary>
        public Vector3 startRotation;

        /// <summary>
        /// reference to the start position
        /// </summary>
        public Vector3 startPosition;

        // Use this for initialization
        void Start()
        {
            startRotation = gameObject.transform.eulerAngles;
            startPosition = gameObject.transform.localPosition;
        }

        /// <summary>
        /// sets the object back to the start position and rotation
        /// </summary>
        public void SetObjectBack()
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            gameObject.transform.eulerAngles = startRotation;
            gameObject.transform.localPosition = startPosition;
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}
