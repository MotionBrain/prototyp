﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeagullReaction : MonoBehaviour
{
    public GameObject seagull;
    public bool isTriggered;

    void Update()
    {
        if(isTriggered)
        {
            seagull.GetComponent<Animator>().SetBool("IsTriggered", true);
        }
        else
        {
            seagull.GetComponent<Animator>().SetBool("IsTriggered", false);
            GetComponent<SeagullReaction>().enabled = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name != "Box001")
        {
            GetComponent<SeagullReaction>().enabled = true;
            isTriggered = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        isTriggered = false;
        
    }
}
