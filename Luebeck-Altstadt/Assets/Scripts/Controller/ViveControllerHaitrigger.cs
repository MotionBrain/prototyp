﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
namespace JoachimLipka
{
    /// <summary>
    /// 
    /// </summary>
    public class ViveControllerHaitrigger : MonoBehaviour
    {
        //reference to the object being tracked 
        private SteamVR_TrackedObject trackedObj;


        public GameObject PathController;

        //access to the vive controller
        private SteamVR_Controller.Device Controller
        {
            get { return SteamVR_Controller.Input((int)trackedObj.index); }
        }

        void Awake()
        {
            trackedObj = GetComponent<SteamVR_TrackedObject>();
        }


        private void Start()
        {
            for (int i = 0; i < 2; i++)
            {
                PathController.GetComponent<SendShip>().city[i] = null;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Controller.GetHairTriggerDown())
            {
                // grap object
                if(gameObject.GetComponent<ControllerInteract>().collidingObject)
                {
                    gameObject.GetComponent<ControllerInteract>().GrabObject();
                }

                // starts the trade game object
                if (gameObject.GetComponent<ControllerInteract>().startTradeGame)
                {
                    gameObject.GetComponent<ControllerInteract>().startTradeGame.GetComponent<StartTradingGame>().StartGame();
                }

                // turn lights on
                if (gameObject.GetComponent<ControllerInteract>().lightObject)
                {
                    gameObject.GetComponent<ControllerInteract>().lightObject.GetComponent<TurnOnLights>().TurnOnLight();
                }

                // interacts with the city
                if (gameObject.GetComponent<ControllerInteract>().cityObject)
                {
                    if (PathController.GetComponent<SendShip>().city[0] == null)
                    {
                        PathController.GetComponent<SendShip>().city[0] = gameObject.GetComponent<ControllerInteract>().cityObject;
                    }
                    else if (PathController.GetComponent<SendShip>().city[1] == null
                        || PathController.GetComponent<SendShip>().city[0].name != PathController.GetComponent<SendShip>().city[1].name)
                    {
                        PathController.GetComponent<SendShip>().city[1] = gameObject.GetComponent<ControllerInteract>().cityObject;
                        PathController.GetComponent<SendShip>().SendResources();
                        for (int i = 0; i < 2; i++)
                        {
                            PathController.GetComponent<SendShip>().city[i] = null;
                        }
                    }
                }
            }

            // 3
            if (Controller.GetHairTriggerUp())
            {
                if (gameObject.GetComponent<ControllerInteract>().objectInHand)
                {
                    gameObject.GetComponent<ControllerInteract>().ReleaseObject();
                }
            }
        }
    }
}
