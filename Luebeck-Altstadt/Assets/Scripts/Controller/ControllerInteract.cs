﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JoachimLipka
{
    public class ControllerInteract : MonoBehaviour
    {

        private SteamVR_TrackedObject trackedObj;

        /// <summary>
        /// reference to the collider object
        /// </summary>
        public GameObject collidingObject;

        /// <summary>
        /// reference to the city object
        /// </summary>
        public GameObject cityObject;

        /// <summary>
        /// reference to the object that the user is currently grabbing
        /// </summary>
        public GameObject objectInHand;

        /// <summary>
        /// reference to the light object
        /// </summary>
        public GameObject lightObject;

        /// <summary>
        /// reference to the start object, which starts the trade game
        /// </summary>
        public GameObject startTradeGame;

        private SteamVR_Controller.Device Controller
        {
            get { return SteamVR_Controller.Input((int)trackedObj.index); }
        }

        void Awake()
        {
            trackedObj = GetComponent<SteamVR_TrackedObject>();
        }

        /// <summary>
        /// This method accepts a collider as a parameter and uses its GameObject as the collidingObject for grabbing and releasing. Moreover, it:
        /// </summary>
        /// <param name="other"></param>
        private void SetCollidingObject(Collider other)
        {
            // Assigns the object as a potential city object target
            if (other.gameObject.layer.ToString() == "9")
            {
                cityObject = other.gameObject;
            }

            // Assigns the object as a potential grab target
            if (other.gameObject.layer.ToString() == "10")
            {
                collidingObject = other.gameObject;
            }

            // Assigns the object as a potential light target
            if (other.tag.ToString() == "Light")
            {
                lightObject = other.gameObject;
            }

            // Assigns the object as a potential light target
            if (other.tag.ToString() == "StartTradeGame")
            {
                startTradeGame = other.gameObject;
            }

        }

        public void OnTriggerEnter(Collider other)
        {
            SetCollidingObject(other);
        }

        public void OnTriggerStay(Collider other)
        {
            SetCollidingObject(other);
        }


        public void OnTriggerExit(Collider other)
        {
            cityObject = null;
            collidingObject = null;
        }

        public void GrabObject()
        {
            objectInHand = collidingObject;
            collidingObject = null;
            var joint = AddFixedJoint();
            joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
        }

        private FixedJoint AddFixedJoint()
        {
            FixedJoint fx = gameObject.AddComponent<FixedJoint>();
            fx.breakForce = 20000;
            fx.breakTorque = 20000;
            return fx;
        }

        public void ReleaseObject()
        {
            if (GetComponent<FixedJoint>())
            {
                GetComponent<FixedJoint>().connectedBody = null;
                Destroy(GetComponent<FixedJoint>());
                objectInHand.GetComponent<Rigidbody>().velocity = Controller.velocity;
                objectInHand.GetComponent<Rigidbody>().angularVelocity = Controller.angularVelocity;
            }
            objectInHand = null;
        }
    }
}
