﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace JoachimLipka
{
    /// <summary>
    /// 
    /// </summary>
    public class Resources : MonoBehaviour
    {
        /// <summary>
        /// the pased time to get more resources
        /// </summary>
        public float woodTimer;

        public GameObject luebeckStorage;

        private void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            woodTimer += Time.deltaTime;
            if(woodTimer > 2)
            {
                luebeckStorage.GetComponent<CityStorage>().wood += 1;
                woodTimer = 0;
                luebeckStorage.GetComponent<CityStorage>().SetResourcesToUi();
            }
        }
    }
}

